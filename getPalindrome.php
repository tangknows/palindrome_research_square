<?php

use PHPUnit\Framework\TestCase;
require_once('Palindrome.php');

    function getPalindrome()
    {
        $largestPalindrome = null;

        for($a=100; $a<=1000;$a++){
            for ($b=100; $b<=1000; $b++){
                $product = $a * $b;
                $notPalindrome = !Palindrome::isPalindrome($product);
                
                if ($notPalindrome){
                    continue;
                }

                if ($product > $largestPalindrome){
                    $largestPalindrome = $product;
                }
    
            }
        }

        return $largestPalindrome;
    }


    getPalindrome(); // Largest 3 digit palindrome is 906609
            
?>


