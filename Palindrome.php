<?php

    class Palindrome {

        public static function isPalindrome($val) {
            $val = strval($val);
            $valReversed = strval($val);
            
            if ($valReversed == strrev($val)) {
                return true;
            }
            return false;
        }

    }

?>


