<?php
        
use PHPUnit\Framework\TestCase;
require_once('Palindrome.php');

class LargestPalindromeTest extends TestCase {

    public function test_is_palindrome() {
        $results = Palindrome::isPalindrome(9009);
        $this->assertTrue($results);
    }

    public function test_non_palindrome_num() {
        $results = Palindrome::isPalindrome(9082);
        $this->assertFalse($results);
    }

    public function test_palindrome_2_digit_num() {
        $results = Palindrome::isPalindrome(11);
        $this->assertTrue($results);
    }

    public function test_non_palindrome_2_digit_num() {
        $results = Palindrome::isPalindrome(12);
        $this->assertFalse($results);
    }

    public function test_non_palindrome_1_digit_num() {
        $results = Palindrome::isPalindrome(1);
        $this->assertTrue($results);
    }

    public function test_is_palindrome_string_return_true() {
        $results = Palindrome::isPalindrome("racecar");
        $this->assertTrue($results);
    }

    public function test_is_palindrome_string_return_false() {
        $results = Palindrome::isPalindrome("race");
        $this->assertFalse($results);
    }

}